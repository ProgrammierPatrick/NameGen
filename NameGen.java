	//E|ria|dor
	//Il|tha|en
	//El|rond
	//A|rya
	//Lya|tha|en

import java.util.Random;
import java.io.Reader;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileNotFoundException;


public class NameGen {
	public static void main(String[] args) throws FileNotFoundException {
		NameGen names = new NameGen(new FileReader("elfnames.conf"));
		for(int i=0;i<20;++i) {
			System.out.println(names.generateName()+" "+names.generateName());
		}
	}

	private Random rand = new Random();
	private String[] vocals = {"a","e","i","o","ya"};
	private String[] konsonants = {"r","d","l","th","n"};

	public NameGen() {}
	public NameGen(String[] v, String[] k) { vocals = v; konsonants = k; }
	public NameGen(Reader reader) {
		try {
			BufferedReader r = new BufferedReader(reader);
			for(;;) {
				String line = r.readLine();
				if(line == null) break;
				String[] parts = line.split(":");
				if(parts[0].toLowerCase().equals("vocals")) {
					vocals = parts[1].split(" ");
				} else if(parts[0].toLowerCase().equals("konsonants")) {
					konsonants = parts[1].split(" ");
				} else {
					System.err.println("Line not recognized: "+line);
					System.exit(1);
				}
			}
		} catch(IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	private String getVocal() {
		return vocals[rand.nextInt(vocals.length)];
	}
	private String getKonsonant() {
		return konsonants[rand.nextInt(konsonants.length)];
	}
	

	public String generateName() {
		String name = "";
		int length = rand.nextInt(2)+2;
		for(int i=0;i<length;++i) {
			boolean both = rand.nextBoolean();
			boolean konsonantFirst = rand.nextBoolean();
			if(both||konsonantFirst) name += getKonsonant();
			name += getVocal();
			if(both||!konsonantFirst) name += getKonsonant();
		}
		return (char)(name.charAt(0)+'A'-'a') + name.substring(1);
	}

}
