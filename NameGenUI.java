import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class NameGenUI extends JFrame implements ActionListener {
	JTextField v = new JTextField("a e i o ya");
	JTextField k = new JTextField("r d l th n");

	JTextArea results = new JTextArea();
	
	JTextArea out = new JTextArea();

	public static void main(String[] args) {
		NameGenUI ui = new NameGenUI();
	}

	public void actionPerformed(ActionEvent e) {
		NameGen gen = new NameGen(v.getText().split(" "), k.getText().split(" "));
		String str = "";
		for(int i=0;i<20;++i) {
			str += gen.generateName() + " " + gen.generateName() + "\n";
		}
		results.setText(str);
	}

	public NameGenUI() {
		super("Name Gen");
		setSize(1600, 900);

		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(2,1));
		panel.add(v);
		panel.add(k);

		setLayout(new BorderLayout());

		add(panel, BorderLayout.NORTH);
		add(results, BorderLayout.CENTER);
		
		JButton button = new JButton("Generate!");
		button.addActionListener(this);
		add(button, BorderLayout.SOUTH);

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
}

